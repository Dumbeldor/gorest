package entity

import (
	"fmt"
	"time"

	"github.com/Dumbeldor/gorest"
)

// User struct used by ORM
type User struct {
	ID                     int64     `json:"id" gorm:"primary_key:true"`
	Username               string    `json:"username" gorm:"type:varchar(21);unique_index;not null;" validate:"required,min=3,max=20"`
	Email                  string    `json:"email" gorm:"type:varchar(255);unique;not null" validate:"required,email"`
	Password               string    `json:"password" validate:"required,min=8"`
	Salt1                  string    `gorm:"type:varchar(500)"`
	Salt2                  string    `gorm:"type:varchar(500)"`
	CreatedAt              time.Time `gorm:"default:now()" json:"createdAt"`
	IsLocked               bool      `gorm:"default:false"`
	Friends                []*User   `gorm:"many2many:friends;association_jointable_foreignkey:friend_id"`
	FriendsRequestSent     []*User   `gorm:"many2many:friends_sent;association_jointable_foreignkey:friend_id"`
	FriendsRequestReceived []*User   `gorm:"many2many:friends_received;association_jointable_foreignkey:friend_id"`
}

func (u *User) Validate() error {
	// Username validate
	if len(u.Username) < 3 {
		return fmt.Errorf("3 characters is the minimum username length")
	}
	if len(u.Username) > 20 {
		return fmt.Errorf("20 characters is the maximum username length")
	}

	if err := gorest.GetValidate().Var(u.Email, "required,email"); err != nil {
		return fmt.Errorf("invalid email")
	}
	return nil
}

func (u *User) String() string {
	return fmt.Sprintf("User<%d %s %s>", u.ID, u.Username, u.Email)
}
