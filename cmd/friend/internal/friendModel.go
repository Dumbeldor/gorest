package internal

import (
	"github.com/Dumbeldor/gorest"
	"github.com/jinzhu/gorm"
	log "github.com/sirupsen/logrus"
	"gitlab.com/Dumbeldor/gorest/entity"
)

func getFriends(userID int64) (*[]Friend, error) {
	var friends []Friend

	err := gorest.GetDB().NativeDB.
		Table("friends").
		Select("users.id, users.username").
		Joins("INNER JOIN users on friends.friend_id = users.id").
		Where("friends.user_id = ?", userID).
		Scan(&friends).
		Error

	if err != nil && !gorm.IsRecordNotFoundError(err) {
		log.Errorln(err)
		return nil, err
	}

	return &friends, nil
}

func addFriendRequest(session *gorest.Session, userID int64) error {
	user := &entity.User{ID: session.UserID}
	userRequest := &entity.User{ID: userID}

	err := gorest.GetDB().NativeDB.
		Model(user).
		Association("FriendsRequestSent").
		Append(userRequest).
		Error
	if err != nil {
		log.Errorln(err)
		return err
	}

	err = gorest.GetDB().NativeDB.
		Model(userRequest).
		Association("FriendsRequestReceived").
		Append(user).
		Error
	if err != nil {
		log.Errorln(err)
		return err
	}

	return nil
}

func alreadyFriendRequest(session *gorest.Session, userID int64) (bool, error) {
	err := gorest.GetDB().NativeDB.
		Table("friends_sent").
		Select("1").
		Where("user_id = ? && friend_id = ?", session.UserID, userID).
		Scan(&entity.Friend{}).
		Error

	if gorm.IsRecordNotFoundError(err) {
		return false, nil
	}
	if err != nil {
		log.Errorln(err)
		return false, err
	}

	return true, nil
}

func alreadyFriend(session *gorest.Session, userID int64) (bool, error) {
	err := gorest.GetDB().NativeDB.
		Table("friends").
		Select("1").
		Where("user_id = ? && friend_id = ?", session.UserID, userID).
		Scan(&entity.Friend{}).
		Error

	if gorm.IsRecordNotFoundError(err) {
		return false, nil
	}
	if err != nil {
		log.Errorln(err)
		return false, err
	}

	return true, nil
}

func addFriend(session *gorest.Session, userID int64) error {
	user := &entity.User{ID: session.UserID}
	userRequest := &entity.User{ID: userID}

	err := gorest.GetDB().NativeDB.
		Model(user).
		Association("Friends").
		Append(userRequest).
		Error
	if err != nil {
		log.Errorln(err)
		return err
	}

	err = gorest.GetDB().NativeDB.
		Model(userRequest).
		Association("Friends").
		Append(user).
		Error
	if err != nil {
		log.Errorln(err)
		return err
	}

	return nil
}