package internal

import (
	"github.com/labstack/echo/v4"
	"github.com/Dumbeldor/gorest"
	"net/http"
	"strconv"
)

func getFriendsAction(c echo.Context) error {
	session := gorest.GetSession(c)
	var fr friendsResponse
	var err error
	fr.Friends, err = getFriends(session.UserID)

	if err != nil {
		return gorest.Error500(c, "Unknown error to get friends")
	}
	return c.JSON(http.StatusOK, fr)
}

func addFriendRequestAction(c echo.Context) error {
	session := gorest.GetSession(c)
	userID, err := strconv.ParseInt(c.Param("userID"), 10, 64)
	if err != nil {
		return gorest.Error400(c, "User id should be a integer")
	}

	if session.UserID == userID {
		return gorest.Error400(c, "You try to add you, are you alone ? :'(")
	}

	isAlreadyFriendRequest, err := alreadyFriendRequest(session, userID)
	if err != nil {
		return gorest.Error500(c, "Unknown error to check if friend is already requested")
	}
	if isAlreadyFriendRequest {
		return gorest.Error400(c, "You have already sent a friend request to this user")
	}

	isAlreadyFriend, err := alreadyFriend(session, userID)
	if err != nil {
		return gorest.Error500(c, "Unknown error to check if you have already this friend")
	}
	if isAlreadyFriend {
		return gorest.Error400(c, "You are already friend with this user")
	}

	err = addFriendRequest(session, userID)
	if err != nil {
		return gorest.Error500(c, "Error to send friend request")
	}

	return c.JSON(http.StatusOK, "success")
}

func acceptFriendAction(c echo.Context) error {
	session := gorest.GetSession(c)
	userID, err := strconv.ParseInt(c.Param("userID"), 10, 64)
	if err != nil {
		return gorest.Error400(c, "User id should be a integer")
	}

	if session.UserID == userID {
		return gorest.Error400(c, "You try to accept yourself, are you alone ? :'(")
	}

	err = addFriend(session, userID)
	if err != nil {
		return gorest.Error500(c, "Error to accept friend request")
	}

	return c.JSON(http.StatusOK, "success")
}