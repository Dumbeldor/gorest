package internal

type Friend struct {
	Id       int64  `json:"id"`
	Username string `json:"username"`
}
type friendsResponse struct {
	Friends *[]Friend `json:"friends"`
}
