package internal

import (
	"github.com/labstack/echo/v4"
)

func GetRoutes(e *echo.Echo) {
	e.GET("v1/friends", getFriendsAction)
	e.POST("v1/friend/:userID", addFriendRequestAction)
	e.POST("v1/friend/accept/:userID", acceptFriendAction)
}
