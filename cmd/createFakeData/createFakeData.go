package main

import (
	log "github.com/sirupsen/logrus"
	"github.com/Dumbeldor/gorest"
)

func main() {
	// Log
	log.SetFormatter(&log.TextFormatter{ForceColors: true})

	// Config
	gorest.LoadConfig()

	// Connect to DB
	gorest.NewDb()
	defer gorest.GetDB().Close()
	gorest.GetDB().CreateSchema()
	gorest.GetDB().CreateFakeData()
}
