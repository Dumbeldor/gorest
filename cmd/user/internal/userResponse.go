package internal

type loginResponse struct {
	Token string `json:"token"`

	// User ID
	// required: true
	UserID int64 `json:"userId"`
}

type searchResponse struct {
	Id       int64  `json:"id"`
	Username string `json:"username"`
}
