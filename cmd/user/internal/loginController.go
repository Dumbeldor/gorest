package internal

import (
	"github.com/Dumbeldor/gorest"
	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
	"gitlab.com/Dumbeldor/gorest/entity"
	"net/http"
)

func loginAction(c echo.Context) error {
	userRequest := new(entity.User)

	if err := c.Bind(userRequest); err != nil {
		log.Errorln(err)
		return gorest.Error400(c, err.Error())
	}

	userDatabase, err := getUser(userRequest.Username)
	if err != nil || userDatabase.ID == 0 {
		return gorest.Error400(c, "Unknown username")
	}

	if userDatabase.IsLocked {
		log.Errorf("User %s tried to auth but account is locked.\n", userRequest.Username)
		return gorest.Error403(c, "Account locked.")
	}

	encodedPassword := gorest.EncodePassword(
		userRequest.Username, userRequest.Password, userDatabase.Salt1, userDatabase.Salt2,
	)

	if encodedPassword != userDatabase.Password {
		log.Errorf("Invalid password for user %s.\n", userRequest.Username)
		return gorest.Error403(c, "Invalid user/password.")
	}

	var lr loginResponse
	lr.Token, err = gorest.CreateJWT(userRequest.Username, userDatabase.ID)
	if err != nil {
		log.Error(err)
		return gorest.Error500(c, "Failed to created session")
	}

	lr.UserID = userDatabase.ID

	return c.JSON(http.StatusOK, lr)
}
