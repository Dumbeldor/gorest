package internal

import (
	"github.com/labstack/echo/v4"
)

func GetRoutes(e *echo.Echo) {
	e.GET("v1/users", getUsersAction)
	e.GET("v1/user/search/:username", searchUserAction)
	e.POST("v1/user/login", loginAction)
	e.POST("v1/user/register", registerAction)
}
