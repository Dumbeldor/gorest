package internal

import (
	"github.com/Dumbeldor/gorest"
	"github.com/jinzhu/gorm"
	log "github.com/sirupsen/logrus"
	"gitlab.com/Dumbeldor/gorest/entity"
)

func getUsers() []searchResponse {
	var users []searchResponse
	err := gorest.GetDB().NativeDB.
		Table("users").
		Find(&users).
		Error

	if err != nil {
		log.Errorln(err)
	}
	return users
}

func searchUsers(username string) ([]searchResponse, error) {
	var users []searchResponse
	err := gorest.GetDB().NativeDB.
		Table("users").
		Select("id, username").
		Where("username LIKE ?", "%"+username+"%").
		Find(&users).
		Limit(20).
		Error

	if err != nil && !gorm.IsRecordNotFoundError(err) {
		log.Errorln(err)
		return nil, err
	}

	return users, nil
}

func getUser(username string) (*entity.User, error) {
	var user entity.User
	err := gorest.GetDB().NativeDB.
		Where("username = ?", username).
		First(&user).
		Error

	if err != nil {
		log.Errorln(err)
		return nil, err
	}

	return &user, nil
}

func isUsernameExist(username string) (bool, error) {
	var user entity.User

	err := gorest.GetDB().NativeDB.
		Where("username = ?", username).
		First(&user).
		Error

	if err != nil && !gorm.IsRecordNotFoundError(err) {
		log.Errorln(err)
		return false, err
	}

	return user.ID != 0, nil
}

func isEmailExist(email string) (bool, error) {
	var user entity.User

	err := gorest.GetDB().NativeDB.
		Where("email = ?", email).
		First(&user).
		Error

	if err != nil && !gorm.IsRecordNotFoundError(err) {
		log.Errorln(err)
		return false, err
	}

	return user.ID != 0, nil
}

func createUser(user *entity.User) error {
	isPrimaryKeyIsBlank := gorest.GetDB().NativeDB.NewRecord(user)
	if !isPrimaryKeyIsBlank {
		log.Errorln("Unable to create user because primary key need to be blank")
	}

	err := gorest.GetDB().NativeDB.Create(&user).Error
	if err != nil {
		log.Errorln(err)
		return err
	}
	return nil
}
