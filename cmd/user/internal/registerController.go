package internal

import (
	"github.com/Dumbeldor/gorest"
	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
	"gitlab.com/Dumbeldor/gorest/entity"
	"math/rand"
	"time"
)

const (
	pwSaltBytes = 256
)

func registerAction(c echo.Context) error {
	u := new(entity.User)

	if err := c.Bind(u); err != nil {
		log.Errorln(err)
		return gorest.Error400(c, err.Error())
	}

	if err := u.Validate(); err != nil {
		log.Warnln(err)
		return gorest.Error400(c, err.Error())
	}

	isUsernameAlreadyExist, err := isUsernameExist(u.Username)
	if err != nil {
		return gorest.Error500(c, err.Error())
	}
	if isUsernameAlreadyExist {
		return gorest.Error400(c, "Username already taken.")
	}

	isEmailAlreadyExist, err := isEmailExist(u.Email)
	if err != nil {
		return gorest.Error500(c, err.Error())
	}
	if isEmailAlreadyExist {
		return gorest.Error400(c, "Email already taken.")
	}

	salt1, salt2 := generateSalt()

	encodedPassword := gorest.EncodePassword(u.Username, u.Password, salt1, salt2)
	if encodedPassword == "" {
		return gorest.Error500(c, "Unable to encode password.")
	}

	u.Password = encodedPassword
	u.Salt1 = salt1
	u.Salt2 = salt2

	if err := createUser(u); err != nil {
		log.Error(err)
		return gorest.Error500(c, "Unable to create user.")
	}

	return gorest.Write201(c, "Registration succeed!")
}

func generateSalt() (string, string) {
	rand.Seed(time.Now().UnixNano())
	salt1 := gorest.GenerateSalt(pwSaltBytes)
	salt2 := gorest.GenerateSalt(pwSaltBytes)
	return salt1, salt2
}

