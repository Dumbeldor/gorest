package internal

import (
	"fmt"
	"github.com/Dumbeldor/gorest"
	"github.com/labstack/echo/v4"
	"net/http"
)

func getUsersAction(c echo.Context) error {
	users := getUsers()
	return c.JSON(http.StatusOK, users)
}

func searchUserAction(c echo.Context) error {
	username := c.Param("username")
	if username == "" {
		return c.JSON(http.StatusOK, "")
	}

	users, err := searchUsers(username)
	if err != nil {
		return gorest.Error500(c, "Unknown error to search user")
	}
	if users == nil {
		return gorest.Error400(c, fmt.Sprintf("%s user doesnt exist", username))
	}

	return c.JSON(http.StatusOK, users)
}

