package main

import (
	"fmt"
	"github.com/Dumbeldor/gorest"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/Dumbeldor/gorest/cmd/user/internal"
	"net/http"

	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
)

func main() {
	// Log
	log.SetFormatter(&log.TextFormatter{ForceColors: true})

	// Config
	gorest.LoadConfig()

	// Echo instance
	e := echo.New()

	// Connect to DB
	gorest.NewDb()
	defer gorest.GetDB().Close()

	// Middleware
	//e.Use(middleware.Logger())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))

	// Routes
	internal.GetRoutes(e)

	// Start server
	log.Fatal(e.Start(fmt.Sprintf(":%s", gorest.Get("port"))))
}
